## read and manipulate data ####

## open data ####

data_1 = read_csv(file = "./data/subj_1.csv")
data_2 = read_csv(file = "./data/subj_2.csv")


## look into staircase data ####

data_staircase_1 = data_1 %>%
  # select  staircase variables
  select(contains("staircase")) %>%
  # drop_na values
  drop_na() %>%
  # add column containing subj number (it was 7)
  mutate(subject = 7)

data_staircase_2 = read_csv(file = "./data/subj_2.csv")%>%
  # select  staircase variables
  select(contains("staircase")) %>%
  # drop_na values
  drop_na() %>%
  # add column containing subj number (it was 7)
  mutate(subject = 5)

# combine both data sets and make it pretty ####
data_staircase = data_staircase_1 %>%
  bind_rows(data_staircase_2) %>%
  # drop unecessary columns
  select(-c(staircase_loop.stepSize, key_resp_staircase.rt))%>%
  # rename variables
  rename(trial = staircase_loop.thisTrialN, intensity = staircase_loop.intensity, response = staircase_loop.response)%>%
  # make it understandable
  mutate(threshold = ifelse(response == 1,"above","below")) %>%
  mutate(subject = as.factor(subject))

# remove unnecessary data from workspace
rm(data_staircase_1,data_staircase_2)
  


# look into the rest of the data ####

data_task = data_1 %>%
  # combine datasets
  bind_rows(data_2) %>%
  # select interesting variables
  select(c(participant,category, id_stim, contains("key_resp_audio"), contains("key_resp_visual")))%>%
  select(-c(contains("keys")))%>%
  # drop staircase rows
  filter(!is.na(category))%>%
  # create new variables and drop previous ones
  mutate(accuracy = ifelse(is.na(key_resp_audio.corr),key_resp_visual.corr, key_resp_audio.corr))%>%
  mutate(rt = ifelse(is.na(key_resp_audio.rt),key_resp_visual.rt,key_resp_audio.rt))%>%
  select(-c(contains("key_resp")))%>%
  # get stim and modality
  separate(id_stim, into=c("a","b","c","d","e","f"), sep = "[_.]")%>%
  mutate(modality = ifelse(f == "wav", "audio", "visual"))%>%
  mutate(stim_number = ifelse(modality == "audio", as.numeric(e)+1, as.numeric(b)))%>%
  select(-c(a,b,c,d,e,f))

# summarise data ####
summary(data_task)

data_summary = data_task %>%
  group_by(modality, category)%>%
  summarise(mean_rt = mean(rt), sd_rt = sd(rt), mean_acc = mean(accuracy), sd_acc = sd(accuracy))
  
# what type are variables and how to change them ####
class(data_task$participant)
class(data_task$category)
class(data_task$accuracy)
class(data_task$rt)
class(data_task$modality)
class(data_task$stim_number)

data_task = data_task %>%
  mutate(modality = as.factor(modality), stim_number = as.integer(stim_number))

summary(data_task)

## process data for ploting curves

data_curve_audio = data_task %>%
  # keep audio modality only
  filter(modality == "audio") %>%
  # define response given by hte subject
  mutate(response = ifelse(accuracy == 1,category, 
                           ifelse(category == "sh","s","sh")))%>%
  # group
  group_by(participant,stim_number) %>%
  # count number of response in each group
  count(response)%>%
  spread(response,n, fill = 0)%>%
  transmute(percent_s = s/(s+sh))%>%
  ungroup()


data_curve_visual = data_task %>%
  # keep visual modality only
  filter(modality == "visual") %>%
  # define response given by hte subject
  mutate(response = ifelse(accuracy == 1,category, 
                           ifelse(category == "vert","bleu","vert")))%>%
  # group
  group_by(participant,stim_number) %>%
  # count number of response in each group
  count(response)%>%
  spread(response,n, fill = 0)%>%
  transmute(percent_v = vert/(vert+bleu))%>%
  ungroup()

# combine variables from both dataset
data_curve = data_curve_audio%>%
  full_join(data_curve_visual)%>%
  gather(3,4, key = "modality", value = "percent")%>%
  mutate(participant = as.factor(as.character(participant)))

# clean workspace
rm(data_curve_audio, data_curve_visual)